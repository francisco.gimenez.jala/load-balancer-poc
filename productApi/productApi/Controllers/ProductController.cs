using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace productApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private List<Product> _products = new List<Product>()
        {
            new Product(1,"Cellphone", "Tech", 2000),
            new Product(2,"Wall charger", "Tech", 200),
            new Product(3,"Laptop", "Tech", 5000),
            new Product(4,"Bottle of water", "Essentials", 5),
            new Product(5,"Canned Beans", "Essentials", 10),
            new Product(6,"Salt", "Essentials", 2),
            new Product(7,"Coffe", "Essentials", 8)
        };

        
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return _products;
        }

        [HttpGet]
        [Route("{id}")]
        public Product GetById(int id)
        {
            return _products.First(p => p.Id == id);
        }
    }
}